			<div id="content">
				<h2>Kasutajaks registreerimine</h2>				
				<p>Kõik väljad on kohustuslikud.<br />
				Parool peab olema vähemalt neli (4) tähemärki pikk.<p>
				
				<form action="#" method="POST">
					<input type="text" name="regNimi" placeholder="Nimi"><br />
					<input type="text" name="regUser" placeholder="Soovitud kasutajanimi"><br />
					<input type="password" name="regPass" placeholder="Parool"><br />
					<button type="submit" class="nupp">Registreeri</button>
				</form>
				
				<?php
					uusKasutaja();
				?>
			
			</div>