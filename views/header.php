<!DOCTYPE HTML>
<html>
	<head> 
		<meta charset="UTF-8"/>
		<title>Kodulehekülg</title> 
		<link rel="stylesheet" type="text/css" media="screen" href="sheet.css" title="StyleSheet">
		<script src="script.js" type="text/javascript"></script>
	</head>
	
	<body>
		<?php
			lisaInfo();
			privaatneLeht();
		?>
		<div id="wrap">
			<header>
				<h1>See On Lehekülg</h1>
				<ul id="navbar">
					<?php 
						navbar($page);
					?>
				</ul>
			</header>
		
			<div id="sidebar"> 
				<h2>Sidebar</h2>
				<ul>
					<li><a href="#">Esimene</a></li>
					<li><a href="#">Teine</a></li>
					<li><a href="#">Kolmas</a></li>
					<li><a href="#">Neljas</a></li>
				</ul>	
				<?php
					vahetaNuppu();
				?>
			</div>