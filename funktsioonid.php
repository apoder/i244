<?php

	function kontrolliSessiooni() {
		if (isset($_SESSION["roll"]) && $_SESSION["rollNr"] != 1)
			return 1;
		elseif (isset($_SESSION["roll"]))
			return 2;
		else 
			return 0;
	}	
	
	function navbar($page) {
		$link = baas();
		$paring = "SELECT kirjeldus, jrknr, privaatne, adre FROM apoder_menyy ORDER BY jrknr";
		$tulemus = mysqli_query($link, $paring) or die(mysqli_error($link));	
		
		while ($rida = mysqli_fetch_row($tulemus)) {
			if ($rida[2] == 1 && kontrolliSessiooni() == 0) 
				continue;
			if ($rida[3] == "avaleht")
				$rida[3] = "";
			if ($rida[3] == $page)
				echo '<li id="current"><a href="index.php?id='.$rida[3].'">'.$rida[0]."</a></li>\n";
			else
				echo '<li><a href="index.php?id='.$rida[3].'">'.$rida[0]."</a></li>\n";
		}
	}

	function privaatneLeht() {
		$link = baas();
		if (kontrolliSessiooni() == 0 && isset($_GET["id"])) {
			$paring = "SELECT adre, privaatne FROM apoder_menyy WHERE privaatne = 1 AND adre = '".mysqli_real_escape_string($link, $_GET["id"])."'";
			$tulemus = mysqli_query($link, $paring) or die(mysqli_error($link));
		//	$rida = mysqli_fetch_row($tulemus);
			$ridaArv = mysqli_num_rows($tulemus);
			
		//	if ($_GET["id"] == $rida[0] && $rida[0] != "") 
			if ($ridaArv == 1) 
				header("Location: index.php?id=teade&teade=1");
		}
	}
	
	function logiSisse() {
		if (kontrolliSessiooni() == 0) {
			if (isset($_POST["user"]) && isset($_POST["pass"])) {
				$link = baas();
				$paring  = "SELECT user FROM apoder_kasutajad WHERE user = '".mysqli_real_escape_string($link, $_POST["user"])."'; ";
				$paring .= "SELECT A.user, A.pass, A.roll, A.nimi, B.rnimi, A.id FROM apoder_kasutajad A JOIN apoder_roll B WHERE A.roll = B.id AND A.user = '".mysqli_real_escape_string($link, $_POST["user"])."' AND A.pass = sha1('".mysqli_real_escape_string($link, $_POST["pass"])."')";
				
				mysqli_multi_query($link, $paring) or die(mysqli_error($link));
				$tulemus = mysqli_store_result($link);
			//  $ridaUser = mysqli_fetch_row($tulemus);
				$ridaUser = mysqli_num_rows($tulemus);

				mysqli_next_result($link);
				$tulemus = mysqli_store_result($link);
				$rida = mysqli_fetch_row($tulemus);
				$ridaArv = mysqli_num_rows($tulemus);

				
				if ($_POST["user"] == "" || $_POST["pass"] == "") 
					echo '<div class = "error">Täida kõik väljad</div>';
			//	elseif ($_POST["user"] != $ridaUser[0])
				elseif ($ridaUser < 1)	
					echo '<div class = "error">Sellist kasutajanime pole loodud</div>';
			//	elseif (sha1($_POST["pass"]) != $rida[1])
				elseif ($ridaArv < 1)
					echo '<div class = "error">Vale parool</div>';
			//	elseif ($_POST["user"] == $ridaUser[0] && sha1($_POST["pass"]) == $rida[1]) {
				elseif ($ridaArv == 1) {
					$_SESSION["roll"] = $rida[4];
					$_SESSION["user"] = $rida[0];
					$_SESSION["nimi"] = $rida[3];
					$_SESSION["rollNr"] = $rida[5];
					header("Location: index.php?id=teade&teade=3");				
				}
			}	
		} else header("Location: index.php?id=teade&teade=5");
	}
	
	function logiValja() {
			session_destroy();
			header("Location: index.php?id=teade&teade=8");
	}
	
	function vahetaNuppu() {
		if (kontrolliSessiooni() > 0 )
			echo '<div id="login"><a href="index.php?id=logout">Logi välja</a></div>';
		else
			echo '<div id="login"><a href="index.php?id=login">Sisse logimine</a></div>';
	}

	function lisaInfo() {
		if (kontrolliSessiooni() == 1) {
			echo '<div id="info">Tere <b>'.$_SESSION["nimi"].'</b>! Sinu kasutajanimi on <b>'.$_SESSION["user"].'</b> ja roll on <b>'.$_SESSION["roll"].'</b>.
			<a href="index.php?id=muuda">Muuda parooli</a></div>';
		} elseif (kontrolliSessiooni() == 2) {
			echo '<div id="info">Tere <b>'.$_SESSION["nimi"].'</b>! Sinu kasutajanimi on <b>'.$_SESSION["user"].'</b> ja roll on <b>'.$_SESSION["roll"].'</b>.
			<a href="index.php?id=muuda">Muuda parooli</a> | <a href="index.php?id=haldus">Halda kasutajaid</a></div>';
		}
	}

	function suvalinePilt($aadressimuster) {
		$images = glob($aadressimuster);
		$a = array_rand($images);
		$b = $images[$a];
		echo '<img src="'.$b.'" alt="'.$b.'" title="'.$b.'">';
	}
	
	function kuvaTeade() { 
		$link = baas();
		if (isset($_GET["teade"])) {
			$paring = "SELECT id, kirjeldus, tyyp, header FROM apoder_teated WHERE id = ".mysqli_real_escape_string($link, $_GET["teade"]);
			$tulemus = mysqli_query($link, $paring) or die(mysqli_error($link));
			$rida = mysqli_fetch_row($tulemus);
			$ridaArv = mysqli_num_rows($tulemus);
			
		} else header ("Location: index.php");
		
	//	if (isset($_GET["teade"]) && $_GET["teade"] == $rida[0]) {
		if (isset($_GET["teade"]) && $ridaArv == 1) {
			echo '<div class = "'.$rida[2].'">'.$rida[1].'</div>';
			header("Refresh: 3; index.php?id=".$rida[3]);
		} else header("Location: index.php");
	}
	
	function uusKasutaja() {
		if (kontrolliSessiooni() == 0) {
			if (isset($_POST["regNimi"]) && isset($_POST["regUser"]) && isset($_POST["regPass"])) {
				$link = baas();
				$paring = "SELECT user FROM apoder_kasutajad WHERE user = '".mysqli_real_escape_string($link, $_POST["regUser"])."'";
				$tulemus = mysqli_query($link, $paring) or die(mysqli_error($link));
			//	$rida = mysqli_fetch_row($tulemus);
				$ridaArv = mysqli_num_rows($tulemus);
								
				if ($_POST["regNimi"] == "" || $_POST["regUser"] == "" || $_POST["regPass"] == "")
					echo '<div class = "error">Täida kõik väljad</div>';
				elseif (strlen($_POST["regPass"]) < 4)
					echo '<div class = "error">Liiga lühike parool</div>';
			//	elseif ($_POST["regUser"] == $rida[0])
				elseif ($ridaArv == 1)
					echo '<div class = "error">Kasutajanimi on juba kasutuses</div>';
				else {
					$paring = "INSERT INTO apoder_kasutajad (user,pass,nimi,roll) VALUES ('".mysqli_real_escape_string($link, $_POST["regUser"])."', sha1('".mysqli_real_escape_string($link, $_POST["regPass"])."'), '".mysqli_real_escape_string($link, $_POST["regNimi"])."', 2)";
					$tulemus = mysqli_query($link, $paring) or die(mysqli_error($link));	
					echo '<div class = "korras">Kasutaja registreerimine õnnestus</div>';
					header("Refresh: 3; index.php?id=login");
				}
			}
		} else header("Location: index.php?id=teade&teade=4");
	}
	
	function muudaParool() {
		if (kontrolliSessiooni() > 0) {
			if (isset($_POST ["pass"]) || isset($_POST["uusPass"]) || isset($_POST["uusPassKordus"])) {
				$link = baas();
				$paring = "SELECT user, pass FROM apoder_kasutajad WHERE user = '".mysqli_real_escape_string($link, $_SESSION["user"])."' AND pass = sha1('".mysqli_real_escape_string($link, $_POST["pass"])."')";
				$tulemus = mysqli_query($link, $paring) or die(mysqli_error($link));	
				$rida = mysqli_fetch_row($tulemus);
				$ridaArv = mysqli_num_rows($tulemus);
			
			//	if (sha1($_POST["pass"]) == $rida[1] && strlen($_POST["uusPass"]) >= 4 && $_POST["uusPass"] == $_POST["uusPassKordus"]) {
				if ($ridaArv == 1 && strlen($_POST["uusPass"]) >= 4 && $_POST["uusPass"] == $_POST["uusPassKordus"]) {
					$paring = "UPDATE apoder_kasutajad SET pass = sha1('".mysqli_real_escape_string($link, $_POST["uusPass"])."') WHERE user = '".mysqli_real_escape_string($link, $_SESSION["user"])."'";
					$tulemus = mysqli_query($link, $paring) or die(mysqli_error($link));
					header("Location: index.php?id=teade&teade=2");
				} elseif ($_POST["pass"] == "" || $_POST["uusPass"] == "" || $_POST["uusPassKordus"] == "")
					echo '<div class = "error">Täida kõik väljad</div>';
			//	elseif (sha1($_POST["pass"]) != $rida[1])
				elseif ($ridaArv < 1)
					echo '<div class = "error">Vale parool</div>';
				elseif (strlen($_POST["uusPass"]) < 4)
					echo '<div class = "error">Liiga lühike parool</div>';
				elseif ($_POST["uusPass"] != $_POST["uusPassKordus"])
					echo '<div class = "error">Paroolid ei kattu</div>';
			}
		} else header("Location: index.php");
	}
	
	function haldaKasutajaid() {
		if (kontrolliSessiooni() == 2) {
			$link = baas();
			$paring = "SELECT A.id, A.user, A.pass, A.nimi, B.rnimi, A.roll FROM apoder_kasutajad A JOIN apoder_roll B ON A.roll = B.id";
			$tulemus = mysqli_query($link, $paring) or die(mysqli_error($link));
			
			echo "<table>
				<caption>Kasutajad</caption>
				<tr><th>Nr</th><th>Kasutajanimi</th><th>Parool</th><th>Nimi</th><th>Roll</th><th></td>&nbsp</tr>";
			while ($rida = mysqli_fetch_row($tulemus)) {
				if ($rida[5] == 2)
					echo "<tr><td>".$rida[0]."</td><td>".$rida[1]."</td><td>&nbsp</td><td>".$rida[3]."</td><td>".$rida[4].'</td><td><a href="index.php?id=remove&del='.$rida[1].'"><img src="img/delete.png" class="imgDel" alt="Kustuta kasutaja" /></a></td></tr>';
				else
					echo "<tr><td>".$rida[0]."</td><td>".$rida[1]."</td><td>&nbsp</td><td>".$rida[3]."</td><td>".$rida[4].'</td><td></td></tr>';
			}
			echo "</table>";
		} else header("Location: index.php?id=teade&teade=1");
	}
	
	function kustutaKasutaja() {
		if (kontrolliSessiooni() == 2) {
			if (isset($_GET["del"])) {
				$link = baas();
				$paring = "SELECT id, user, roll FROM apoder_kasutajad WHERE user = '".mysqli_real_escape_string($link, $_GET["del"])."'";
				$tulemus = mysqli_query($link, $paring) or die(mysqli_error($link));
				$rida = mysqli_fetch_row($tulemus);
				
				if ($rida[2] == 2) {
					$paring = "DELETE FROM apoder_kasutajad WHERE user = '".mysqli_real_escape_string($link, $_GET["del"])."'";
					$tulemus = mysqli_query($link, $paring) or die(mysqli_error($link));
					header("Location: index.php?id=teade&teade=6");
				} else header("Location: index.php?id=teade&teade=7");
			}
		} else header ("Location: index.php?id=teade&teade=9");
	}	
	
	function eiHaki($kuhuSuunan) {
		if(!isset($_SESSION["roll"]) && !isset($_GET["id"]))
		header("Location: $kuhuSuunan");
	}
	
?>