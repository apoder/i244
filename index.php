<?php
	session_start();

	require("db_conf.php");
	require_once("funktsioonid.php");
	
	if (!isset($_GET["id"]) || $_GET["id"] == "" || !file_exists("views/".$_GET["id"].".php") && $_GET["id"] != "logout" && $_GET["id"] != "remove")
		$page = "avaleht";
	elseif ($_GET["id"] == "logout")
		logiValja();
	elseif ($_GET["id"] == "remove")
		kustutaKasutaja();
	else 
		$page = $_GET["id"];

	require_once("views/header.php");
	include("views/".$page.".php");
	require_once("views/footer.php");
?>