CREATE TABLE apoder_kasutajad (
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	user VARCHAR(40) UNIQUE NOT NULL,
	pass VARCHAR(40) NOT NULL,
	nimi VARCHAR(40) NOT NULL,
	roll INTEGER NOT NULL
);

CREATE TABLE apoder_menyy (
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	adre VARCHAR(40) UNIQUE NOT NULL,
	kirjeldus VARCHAR(40) NOT NULL,
	privaatne INTEGER NOT NULL,
	jrknr INTEGER
);

CREATE TABLE apoder_roll (
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	rnimi VARCHAR(20) UNIQUE NOT NULL
);

CREATE TABLE apoder_teated (
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	kirjeldus TEXT NOT NULL,
	tyyp VARCHAR(20) NOT NULL,
	header VARCHAR(20)
);

INSERT INTO apoder_kasutajad (id, user, pass, nimi, roll) VALUES (1, 'admin', sha1('admin'), 'Admin', 1);

INSERT INTO apoder_roll (rnimi) VALUES ('Administraator');
INSERT INTO apoder_roll (rnimi) VALUES ('Tavakasutaja');

INSERT INTO apoder_menyy (adre, kirjeldus, privaatne, jrknr) VALUES ('avaleht', 'Avaleht', 0, 10);
INSERT INTO apoder_menyy (adre, kirjeldus, privaatne, jrknr) VALUES ('kolm', 'Kolm', 0, 30);
INSERT INTO apoder_menyy (adre, kirjeldus, privaatne, jrknr) VALUES ('kaks', 'Kaks', 0, 20);
INSERT INTO apoder_menyy (adre, kirjeldus, privaatne, jrknr) VALUES ('neli', 'Peidus', 1, 40);

INSERT INTO apoder_teated (id, kirjeldus, tyyp, header) VALUES (1, 'Selle lehe nägemiseks pead oleme sisse logitud', 'error', 'login');
INSERT INTO apoder_teated (id, kirjeldus, tyyp, header) VALUES (2, 'Parool muudetud', 'korras', 'muuda');
INSERT INTO apoder_teated (id, kirjeldus, tyyp) VALUES (3, 'Sisselogimine õnnestus', 'korras');
INSERT INTO apoder_teated (id, kirjeldus, tyyp) VALUES (4, 'Oled juba kasutajad', 'error');
INSERT INTO apoder_teated (id, kirjeldus, tyyp) VALUES (5, 'Oled juba sisse logitud', 'error');
INSERT INTO apoder_teated (id, kirjeldus, tyyp, header) VALUES (6, 'Kasutaja kustutatud', 'korras', 'haldus');
INSERT INTO apoder_teated (id, kirjeldus, tyyp) VALUES (7, 'Seda kasutajat ei saa kustutada', 'error');
INSERT INTO apoder_teated (id, kirjeldus, tyyp, header) VALUES (8, 'Oled välja logitud', 'korras', 'login');
INSERT INTO apoder_teated (id, kirjeldus, tyyp) VALUES (9, 'Sul ei ole piisavalt õigusi', 'error');